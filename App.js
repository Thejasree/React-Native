/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Text,
    View,
    AppRegistry,
} from 'react-native';
import ModalExample from './Sample/Home';

export default class App extends  Component{
    render(){
        return(
            <ModalExample />
        );
    }
}
AppRegistry.registerComponent('ModalComponent', () => App);