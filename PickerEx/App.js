/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    AppRegistry
} from 'react-native';
import PickerEx from "./Home/SamplePicker";



export default class App extends Component<{}> {
  render() {
    return (
      <PickerEx/>
    );
  }
}


AppRegistry.registerComponent('PickerEx', () => App);