import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Picker
} from 'react-native';

export default class PickerEx extends  Component{
    state = {language: ''};

    updateLanguage = (language) => {
        this.setState({ language: language })
    };
    render(){
        return(
            <View>
                <Picker
                    selectedValue={this.state.language}
                    onValueChange={(itemValue, itemIndex) => this.setState({language: itemValue})}>
                    <Picker.Item label="Java" value="java" />
                    <Picker.Item label="JavaScript" value="js" />
                    <Picker.Item label="CSS" value="css" />
                    <Picker.Item label="React" value="react" />
                    <Picker.Item label="ReactNative" value="rn" />
                </Picker>
                <Text style = {styles.text}>{this.state.language}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    text: {
        fontSize: 30,
        alignSelf: 'center',
        color: 'red'
    }
});