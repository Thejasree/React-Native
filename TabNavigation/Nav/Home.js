
import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';
class Home extends Component {
    render() {
        return (
            <View>
                <Text style = {styles.text}>
                    Home
                </Text>
            </View>
        )
    }
}
export default  Home

const styles = StyleSheet.create({
    text: {
        fontSize: 30,
        textAlign: 'center',
        margin: 10,
    },
});