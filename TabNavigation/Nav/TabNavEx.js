import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Dimensions,

} from 'react-native';
import TabNavigator from 'react-native-tab-navigator';
import Profile from './Profile';
import Home from './Home';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;


export default class TabNavigatorEx  extends Component{
    state= {
        selectedTab: 'home',

    };

    render(){
        return(
            <TabNavigator style={styles.container}>
                <TabNavigator.Item
                    selected={this.state.selectedTab === 'home'}
                    title="Home"
                    selectedTitleStyle={{color: 'Blue'}}
                    onPress={() =>
                        this.setState({selectedTab: 'home'})}>
                    <Home/>

                </TabNavigator.Item>
                <TabNavigator.Item
                    selected={this.state.selectedTab === 'profile'}
                    title="Profile"
                    selectedTitleStyle={{color: 'Blue'}}

                    onPress={() =>
                        this.setState({selectedTab: 'profile'})}>
                    <Profile/>
                </TabNavigator.Item>
            </TabNavigator>


        );
    }
}
const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'lightblue',
    },
});