
import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';
class Profile extends Component {
    render() {
        return (
            <View>
                <Text style = {styles.text}>
                    Profile
                </Text>
            </View>
        )
    }
}
export default Profile
const styles = StyleSheet.create({
    text: {
        fontSize: 30,
        textAlign: 'center',
        margin: 10,

    },
});