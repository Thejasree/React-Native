/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  View,
    Text,
    AppRegistry,
} from 'react-native';
import TabNavigatorEx from './Nav/TabNavEx';

export default class App extends Component {
  render() {
    return (
      <TabNavigatorEx />
    );
  }
}

AppRegistry.registerComponent('TabNavigation', () => App);