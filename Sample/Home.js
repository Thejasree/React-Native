import React, { Component } from 'react';
import {
    Text,
    View,
    AppRegistry,
    Modal,
    TouchableHighlight,
    TouchableOpacity
} from 'react-native';

import  Icon  from 'react-native-vector-icons/FontAwesome';
const myIcon = (<Icon name="close" size={30} color="#900"/>)
export default class ModalExample extends Component<{}> {
    state = {
        modalVisible : false
    }
    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }
    render() {
        return (
            <View style={{marginTop: 22,}}>
                {/*Animation for Modal */}
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {alert("Modal has been closed.")}}
                >
                    <View style={{marginTop: 22}}>
                        {/* Closing Icon of react-native vector icons*/}
                        <View >
                            <TouchableOpacity onPress={()=>this.setModalVisible(false)}>
                                {myIcon}
                                <Text>Close the Modal by clicking on the Icon.</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                {/* Showing the Modal after clicking this*/}
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true)
                }}>
                    <Text>Show Modal</Text>

                </TouchableHighlight>

            </View>
        );
    }

}